
//
//  ViewController.swift
//  TP2_Calculator
//
//  Created by mbds on 08/04/2021.
//

import UIKit

class ViewController: UIViewController {
    
    var txtSaisi : String = ""
    var txtAffiche : String = ""
    
    var nbre1 : String = ""
    var nbre2 : String = ""
    var lastNumber : Double = 0
    var touchePress :  String = ""
    var checkNombre: Bool = false
    var checkEqual: Bool = false
    var operationUser : String =  ""
    var isoperationUser : Bool = false
    var operateurAvant : String = ""
    var resultat1 : Double = 0
    var canContinue : Bool = true
    var result : Double = 0
    var nextOperation : Bool = false
    
    @IBOutlet weak var txtResultat: UITextField!
    

    @IBAction func setText(_ sender: Any) {
        txtSaisi =  (sender as AnyObject).currentTitle!!
        //txtResultat.text = txtAffiche + txtSaisi
        txtAffiche = txtResultat.text!
    }
    
    
    @IBAction func numberButton(_ sender: Any) {
        if(checkEqual && !nextOperation) {
            initChamp()
        }
        checkEqual = false
            if(!checkNombre){
                touchePress = (sender as AnyObject).currentTitle!!
                if(touchePress == "."){
                    if(nbre1.contains(".") || nbre1.count == 0) {
                        touchePress = ""
                    }
                }
                nbre1.append(touchePress)
                txtResultat.text = nbre1
                
            }else{
                touchePress = (sender as AnyObject).currentTitle!!
                if(touchePress == "."){
                    if(nbre2.contains(".")  || nbre2.count == 0) {
                        touchePress = ""
                    }
                }
                nbre2.append(touchePress)
                txtResultat.text = nbre1 + " " + operationUser + " " + nbre2;
                
            }
        
        }
        
        @IBAction func operation(_ sender: Any) {
            let nbre1Test = nbre1
            var nbre2Test = nbre2
            if (nbre2Test.count == 0)  { nbre2Test = "0" }
            if (!isValidNumber(unTexte: nbre1Test) || !isValidNumber(unTexte: nbre2Test)){
                canContinue = false
            }
            
            if (checkEqual) {
                nbre1 = String(result)
                nbre2 = ""
                checkNombre = true
                isoperationUser = true
                nextOperation = true
            }
            
            if(canContinue){
                if(!isoperationUser){
                    //nbre1 = String(lastNumber)
                    
                    touchePress = "0"
                    lastNumber = 0
                    checkNombre = true
                    isoperationUser = true
                    operationUser = (sender as AnyObject).currentTitle!!
                    txtResultat.text = nbre1 + " " + operationUser;
                    operateurAvant = operationUser;
                }
                else{
                    operationUser = (sender as AnyObject).currentTitle!!
                    txtResultat.text = nbre1 + " " + operationUser + " " + nbre2;
                }
            }
        }
        
        @IBAction func equal(_ sender: Any) {
            
            if (isValidNumber(unTexte: nbre1) && isValidNumber(unTexte: nbre2)){
     
                touchePress = "0"
                checkEqual = true
                
                if(operationUser == "+"){
                    
                    result = Double(nbre1)! + Double(nbre2)!
                    txtResultat.text = String(result)
                }
                else if(operationUser == "-"){
                    
                    result = Double(nbre1)! - Double(nbre2)!
                    txtResultat.text = String(result)
                }
                else if(operationUser == "x"){
                    
                    result = Double(nbre1)! * Double(nbre2)!
                    txtResultat.text = String(result)
                }
                else if(operationUser == "/"){
                    if Double(nbre2) == 0{
                        
                        let alert = UIAlertController(title: "Alert", message: "Opération (division par 0) invalide!", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                    }else{
                    result = Double(nbre1)! / Double(nbre2)!
                    txtResultat.text = String(result)
                    }
                }
                nextOperation = false
            }
        }

        @IBAction func clearAll(_ sender: Any) {
            initChamp()
        }
    
        func initChamp() {
            nbre1 = "";
            nbre2 = "";
            lastNumber = 0;
            checkNombre = false;
            isoperationUser = false;
            txtResultat.text = "";
            canContinue = true
            checkEqual = false
            nextOperation = false
        }
    
    func isValidNumber(unTexte: String)-> Bool {
        if let mFloat = Float(unTexte) {
            return true
        }else {
            return false
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    


}
 
